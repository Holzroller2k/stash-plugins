package de.dwpbank.stash.plugin.detectpommerge;

import com.atlassian.stash.content.AbstractChangeCallback;
import com.atlassian.stash.content.Change;
import com.atlassian.stash.content.ChangeType;
import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.pull.PullRequestParticipant;
import com.atlassian.stash.pull.PullRequestService;
import com.atlassian.stash.scm.pull.MergeRequest;
import com.atlassian.stash.scm.pull.MergeRequestCheck;

import java.util.Set;

public class DetectPomMerges implements MergeRequestCheck {

    private static final int REQUIRED_APPROVER = 2;
    private PullRequestService pullRequestService;

    public DetectPomMerges(PullRequestService pullRequestService) {
        this.pullRequestService = pullRequestService;
    }

    @Override
    public void check(final MergeRequest mergeRequest) {
        if(hasPom(mergeRequest.getPullRequest())) {
            int acceptedCount = countParticipants(mergeRequest.getPullRequest().getReviewers()) +
                    countParticipants(mergeRequest.getPullRequest().getParticipants());
            if (acceptedCount < REQUIRED_APPROVER) {
                String vetoMessage = "Merge auf pom.xml erkannt - " + acceptedCount + " von " + REQUIRED_APPROVER + " erforderlichen Approvern!";
                mergeRequest.veto(vetoMessage, vetoMessage);
            }
        }
    }

    private boolean hasPom(PullRequest pr) {
        // Dirty trick to make Java support closures
        final boolean[] hasPom = new boolean[1];
        pullRequestService.streamChanges(pr.getToRef().getRepository().getId(), pr.getId(), new AbstractChangeCallback() {
            public boolean onChange(Change change) {
                String fileName = change.getPath().getName();
                if ("pom.xml".equals(fileName)) {
                    ChangeType type = change.getType();
                    if (type == ChangeType.DELETE || type == ChangeType.MODIFY || type == ChangeType.MOVE || type == ChangeType.UNKNOWN) {
                        hasPom[0] = true;
                        // Stop processing
                        return false;
                    }
                }
                return true;
            }
        });
        return hasPom[0];
    }

    private int countParticipants(Set<PullRequestParticipant> participants) {
        int acceptedCount = 0;
        for (PullRequestParticipant reviewer : participants) {
            acceptedCount += reviewer.isApproved() ? 1 : 0;
        }
        return acceptedCount;
    }
}
